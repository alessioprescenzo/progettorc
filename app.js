var createError = require('http-errors');
var express = require('express');
var session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var MongoClient = require('mongoose');
var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var FBOauth = require('./bin/schema/schema');
var config = require('./config.json');
//Routers
var indexRouter = require('./routes/index');
var fbRouter = require('./routes/fbLogin');

//Inizialize app
var app = express();

//Inizialize database connection
MongoClient.connect(config.db.url, { useNewUrlParser: true, useUnifiedTopology: true });
MongoClient.set('useCreateIndex', true);
var db = MongoClient.connection;
db.on('error', function(error){
  console.log("Error on connection 1");
  console.log(error);
});
db.once('open', function(){
  console.log('Connected to MongoDB on localhost:27017\n\n');
});

//Inizialize FBLogin via passport
passport.use(new FacebookStrategy({
    clientID: config.passport.clientID,
    clientSecret: config.passport.clientSecret,
    callbackURL: 'https://localhost:8443/login/callback',
    profileFields: ['id','displayName','email','photos']
  },
  function(accessToken, refreshToken, profile, done) {  
    FBOauth.findOne({ id: profile.id},function(err,result){
      if(!result){
        var toRet = new FBOauth({
          id: profile.id,
          name: profile.displayName,
          email: profile.emails[0].value? profile.emails[0].value : " ",
          oauth_secret: accessToken
        });
        toRet.save();
        return done(null, toRet);      
      }else{
        return done(null,result);
      }
    });
  })
);

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  FBOauth.findOne({id: user.id},function(err,user){
    done(err,user);
  });
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// setup cookie and session
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
  secret: 'cookie secret',
  name: 'cookie name',
  proxy: true,
  resave: true,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

//applying router
app.use('/', indexRouter);
app.use('/login',fbRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
