var MongoClient = require('mongoose');
var mongooseFieldEncryption = require("mongoose-field-encryption").fieldEncryption;
var Schema = MongoClient.Schema;

var FBOauthSchema = new Schema({
    id: {
        type: Number,
        index: {
            unique: true,
            parse: true
        }
    },
    name: {
        type: String
    },
    email: {
        type: String
    },
    oauth_secret: {
        type: String
    }

});

FBOauthSchema.plugin(mongooseFieldEncryption, { fields: ["oauth_secret"], secret: "12345678" });

var FBOauth = MongoClient.model('FBOauth', FBOauthSchema);

module.exports = FBOauth;