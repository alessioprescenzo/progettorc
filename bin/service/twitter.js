var Twitter = require('twit');
var woeid = require('woeid');
var config = require('../../config.json');
var sendToQueue = require('../../amqp/send.js');
var client = new Twitter({
    consumer_key: config.twitter.consumer_key,
    consumer_secret: config.twitter.consumer_secret,
    access_token: config.twitter.access_token,
    access_token_secret: config.twitter.access_token_secret
  }); 

var trends = async function obtainTrendsTweets(countryid) {
    return new Promise(async resolve => {
      try{
        var promises = [];
        var countryObj = await woeid.getWoeid(countryid.id);
        var countryTrends = await getTrendsTweets(countryObj.woeid);
        for(var i=0;i<5;i++){
          promises.push(getTweets(countryTrends[i].name));
        }
        
        await Promise.all(promises).then(function(data){
          returnTweets = data;
        });
        resolve();
      }catch(e){
        console.log(e);
        returnTweets = null;
        resolve();
      }
    });  
};

function getTweets(name){
  return new Promise(function(resolve){
    try{
      client.get('search/tweets', {q: name.concat(" filter:images"), tweet_mode:"extended",include_entities:"true", count:"2"},async function(err, tweets, response){
        resolve(tweets);
      });         
    }catch(e){
    console.log(e);
    resolve();
  }
  });
}

function getTrendsTweets(idWoeid){
  return new Promise(resolve => {
    client.get('trends/place', {id: idWoeid, count: "5"}, function(error, trends, res) {      
      try{
        var list = trends[0].trends;
        sendToQueue(list);
        resolve(list);
      }catch(e){
        resolve();
      }
    });
  });
}

module.exports = trends;