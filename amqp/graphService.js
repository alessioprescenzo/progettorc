#!/usr/bin/env node
var JSDOM = require('jsdom').JSDOM;
var amqp = require('amqplib/callback_api');
var fs = require('fs');
const { Console } = require('console');

amqp.connect('amqp://localhost/', function(error0, connection) {
  if (error0) {
      throw error0;
  }
  connection.createChannel(function(error1, channel) {
    if (error1) {
    throw error1;
    }

    var queue = 'rpc_queue';
    
    channel.assertQueue(queue, {
      durable: false
    });
    channel.prefetch(1);
    console.log(' [x] Awaiting RPC requests');
    channel.consume(queue, function reply(msg) {
      var list = JSON.parse(msg.content.toString());
      var data = list.map(function(item){
        return [item.name, item.tweet_volume]
      });

      //Anychart initilization + JSDOM
      var jsdom = new JSDOM('<body><div id="container"></div></body>', {runScripts: 'dangerously'});
      var window = jsdom.window;
      var anychart = require('anychart')(window);
      var anychartExport = require('anychart-nodejs')(anychart);
      
      //Turning data into useful infos for Anychart
      var dataSet = anychart.data.set(data);
      var mapping = dataSet.mapAs({x: 0, value: 1});
      var chart = anychart.column();
      var series = chart.column(mapping);

      //Chart settings
      chart.bounds(0, 0, 3200, 2400);
      chart.container('container');
      chart.draw();

      // generate JPG image and save it to a file
      anychartExport.exportTo(chart, 'jpg').then(function(image) {
        var current = new Date();

        channel.sendToQueue(msg.properties.replyTo,
          image, {
          correlationId: msg.properties.correlationId
        });

        fs.writeFile(current.getTime() + 'Chart.jpg', image, function(fsWriteError) {
          if (fsWriteError) {
            channel.nack(msg);
            console.log(fsWriteError);
          } else {
            channel.ack(msg);
            console.log('>> Work n: ' + msg.properties.correlationId + ' complete');
          }
        });

      }, function(generationError) {
        channel.nack(msg);
        console.log(generationError);
      });
    });
  });
});


