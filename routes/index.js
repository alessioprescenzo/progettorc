var express = require('express');
var router = express.Router();
var twitter = require('../bin/service/twitter');

/* GET home page. */
router.get('/', function(req, res, next) {
  user = req.session.user;
  res.render('index');
});

router.get('/twitter', require('connect-ensure-login').ensureLoggedIn('/login'), 
    async function(req,res,next){
        await twitter(req.query).catch(function(err){
        console.log("Error in router /twitter");
        console.log(err);
        });
    res.render('index');
});

router.get('/logout', function(req, res){
  req.logOut();
  req.session.user=null;
  user = null;
  returnTweets = null;
  console.log("Logged out!");
  res.redirect('/');
});

module.exports = router;
