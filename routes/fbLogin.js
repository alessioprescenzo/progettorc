var express = require('express');
var router = express.Router();
var passport = require('passport');

//Get FB Login
router.get('/', passport.authenticate('facebook'));

//Get Info
router.get('/callback', passport.authenticate('facebook', {failureRedirect: '/FBLogin'}),
    function(req,res,next){
        req.session.user = req.user;
        res.redirect('/');
});

module.exports = router;
